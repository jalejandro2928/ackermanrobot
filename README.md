This project describes the use of deep neural networks for solving the task of obstacle avoidance in a car-like robot using the information obtained from distance sensors.In order to control the car-like robot a web application was built along with a software package in Python, that enables the features of open loop control, point tracking and interfacing with hardware that deals with sensing distances and localizing the robot in the world frame. The trained machine learning algorithm based on deep neural networks managed to successfully navigate through low and medium difficulty environments, which is near to the best behavior we can expect from a robot that does not know the map a priori.

Step to run:

1-The mode is virtual What does it mean? the dynamic of the robot and its sensor are simulate in order to test the behavioral of the algorithms to solve the tasks above

1-open the rpi folder with some textor editor like for example vscode or what you want

2-run the jop_start.py script

3-open the web folder and execute the index.html

4- touch the uper left button until virtual-pc
 
5-play with the enviroment

See the videos
